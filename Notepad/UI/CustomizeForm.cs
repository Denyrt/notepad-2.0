﻿using Notepad.Properties;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Notepad.UI
{
    public partial class CustomizeForm : Form
    {
        private readonly Settings _settings = Settings.Default;

        private event Action<Color> TextColorChanged;
        private event Action<Font> TextFontChanged;
        
        public CustomizeForm()
        {
            InitializeComponent();
            TextColorChanged += OnTextColorChanged;
            TextFontChanged += OnTextFontChanged;
            
            fontDialog.Font = _settings.DefaultFont;
            TextFontChanged?.Invoke(fontDialog.Font);

            colorDialog.Color = _settings.DefaultColor;
            TextColorChanged?.Invoke(colorDialog.Color);
        }

        private void OnTextColorChanged(Color color)
        {
            pictureBoxDefaultColor.BackColor = color;
            textBoxRgb.Text = string.Format("{0}; {1}; {2}", color.R, color.G, color.B);
            richTextBoxExample.ForeColor = color;
        }

        private void OnTextFontChanged(Font font)
        {
            textBox1.Text = font.ToString();
            richTextBoxExample.Font = font;
        }

        private void PictureBoxDefaultColor_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() != DialogResult.OK) return;

            pictureBoxDefaultColor.BackColor = colorDialog.Color;
            TextColorChanged?.Invoke(colorDialog.Color);           
        }

        private void ButtonSave_Click(object sender, EventArgs e)
        {
            _settings.DefaultColor = colorDialog.Color;
            _settings.DefaultFont = fontDialog.Font;
            _settings.Save();
            Close();
        }

        private void ButtonFont_Click(object sender, EventArgs e)
        {
            if (fontDialog.ShowDialog() != DialogResult.OK) return;
            
            TextFontChanged?.Invoke(fontDialog.Font);
        }
    }
}
