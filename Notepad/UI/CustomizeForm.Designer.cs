﻿namespace Notepad.UI
{
    partial class CustomizeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomizeForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonFont = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBoxDefaultColor = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxRgb = new System.Windows.Forms.TextBox();
            this.pictureBoxDefaultColor = new System.Windows.Forms.PictureBox();
            this.fontDialog = new System.Windows.Forms.FontDialog();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.richTextBoxExample = new System.Windows.Forms.RichTextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBoxDefaultColor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDefaultColor)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.groupBoxDefaultColor);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(758, 173);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Text";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonFont);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(6, 18);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(368, 144);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Default font";
            // 
            // buttonFont
            // 
            this.buttonFont.Location = new System.Drawing.Point(134, 91);
            this.buttonFont.Name = "buttonFont";
            this.buttonFont.Size = new System.Drawing.Size(91, 29);
            this.buttonFont.TabIndex = 3;
            this.buttonFont.Text = "Edit";
            this.buttonFont.UseVisualStyleBackColor = true;
            this.buttonFont.Click += new System.EventHandler(this.ButtonFont_Click);
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Location = new System.Drawing.Point(12, 30);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(350, 55);
            this.textBox1.TabIndex = 3;
            // 
            // groupBoxDefaultColor
            // 
            this.groupBoxDefaultColor.Controls.Add(this.label1);
            this.groupBoxDefaultColor.Controls.Add(this.textBoxRgb);
            this.groupBoxDefaultColor.Controls.Add(this.pictureBoxDefaultColor);
            this.groupBoxDefaultColor.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBoxDefaultColor.Location = new System.Drawing.Point(380, 18);
            this.groupBoxDefaultColor.Name = "groupBoxDefaultColor";
            this.groupBoxDefaultColor.Size = new System.Drawing.Size(372, 144);
            this.groupBoxDefaultColor.TabIndex = 0;
            this.groupBoxDefaultColor.TabStop = false;
            this.groupBoxDefaultColor.Text = "Default color";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(58, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "RGB";
            // 
            // textBoxRgb
            // 
            this.textBoxRgb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxRgb.Location = new System.Drawing.Point(101, 31);
            this.textBoxRgb.Name = "textBoxRgb";
            this.textBoxRgb.ReadOnly = true;
            this.textBoxRgb.Size = new System.Drawing.Size(255, 27);
            this.textBoxRgb.TabIndex = 1;
            // 
            // pictureBoxDefaultColor
            // 
            this.pictureBoxDefaultColor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxDefaultColor.Location = new System.Drawing.Point(17, 31);
            this.pictureBoxDefaultColor.Name = "pictureBoxDefaultColor";
            this.pictureBoxDefaultColor.Size = new System.Drawing.Size(35, 99);
            this.pictureBoxDefaultColor.TabIndex = 0;
            this.pictureBoxDefaultColor.TabStop = false;
            this.pictureBoxDefaultColor.Click += new System.EventHandler(this.PictureBoxDefaultColor_Click);
            // 
            // fontDialog
            // 
            this.fontDialog.Font = new System.Drawing.Font("Segoe UI", 10.2F);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(624, 403);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(146, 38);
            this.buttonSave.TabIndex = 1;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.ButtonSave_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(472, 403);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(146, 38);
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // richTextBoxExample
            // 
            this.richTextBoxExample.Location = new System.Drawing.Point(12, 191);
            this.richTextBoxExample.Name = "richTextBoxExample";
            this.richTextBoxExample.Size = new System.Drawing.Size(758, 186);
            this.richTextBoxExample.TabIndex = 1;
            this.richTextBoxExample.Text = resources.GetString("richTextBoxExample.Text");
            // 
            // CustomizeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(782, 453);
            this.Controls.Add(this.richTextBoxExample);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "CustomizeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Customize";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBoxDefaultColor.ResumeLayout(false);
            this.groupBoxDefaultColor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDefaultColor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBoxDefaultColor;
        private System.Windows.Forms.PictureBox pictureBoxDefaultColor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxRgb;
        private System.Windows.Forms.FontDialog fontDialog;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.RichTextBox richTextBoxExample;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonFont;
        private System.Windows.Forms.TextBox textBox1;
    }
}