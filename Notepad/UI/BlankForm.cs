﻿using Notepad.Properties;
using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Notepad.UI
{
    public sealed partial class BlankForm : Form
    {
        public bool IsSaved { get; private set; }

        public string FullName { get; set; }

        public string FileName { get; set; }

        public Stream BaseStream { get; private set; }
        private Thread _timeWatcher;

        private BlankForm()
        {
            InitializeComponent();
            
            RichTextBox_Workspace.ForeColor = Settings.Default.DefaultColor;
            RichTextBox_Workspace.Font = Settings.Default.DefaultFont;

            RichTextBox_Workspace.SelectionColor = Settings.Default.DefaultColor;
            RichTextBox_Workspace.SelectionFont = Settings.Default.DefaultFont;
            
            RichTextBox_Workspace.TextChanged += OnTextCountChanged;                       
        }

        private void InitializeEvents()
        {
            RichTextBox_Workspace.TextChanged += RichTextBox_TextChanged;
        }

        public void Save()
        {
            if (BaseStream is null)
            {
                using (var saveFileDialog = new SaveFileDialog())
                {
                    saveFileDialog.Filter = "txt files (*.txt)|*.txt";

                    if (saveFileDialog.ShowDialog() != DialogResult.OK) return;

                    var stream = saveFileDialog.OpenFile();
                    FullName = saveFileDialog.FileName;
                    FileName = FullName.Substring(FullName.LastIndexOf('\\') + 1);
                    BaseStream = stream;
                }
            }

            BaseStream.SetLength(RichTextBox_Workspace.Text.Length);
            BaseStream.Seek(0, SeekOrigin.Begin);

            using (var writer = new StreamWriter(BaseStream, Encoding.Default, RichTextBox_Workspace.Text.Length, true))
            {
                foreach (var line in RichTextBox_Workspace.Lines)
                {
                    writer.WriteLine(line);
                }

                IsSaved = true;
                Text = FileName;
            }
        }

        public void SaveAs()
        {
            using (var saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.Filter = "txt files (*.txt)|*.txt";

                if (saveFileDialog.ShowDialog() != DialogResult.OK) return;

                var stream = saveFileDialog.OpenFile();
                BaseStream?.Dispose();
                BaseStream = stream;
                FullName = saveFileDialog.FileName;
                FileName = FullName.Substring(FullName.LastIndexOf('\\') + 1);
            }
            
            ReadStream(BaseStream);
            IsSaved = true;
            Text = FileName;
        }

        public void CloseWithoutSaving()
        {
            IsSaved = true;
            Close();
        }

        private void ReadStream(Stream strean)
        {
            using (var reader = new StreamReader(strean, Encoding.Default, true, 1, true))
            {
                this.RichTextBox_Workspace.AppendText(reader.ReadToEnd());
            }
        }

        private void BlankForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsSaved) return;

            var dialog = MessageBox.Show("Do you want save file after closing?", "File closing", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

            switch (dialog)
            {
                case DialogResult.Yes:
                    Save();
                    break;
                case DialogResult.Cancel:
                    e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void OnTextCountChanged(object sender, EventArgs e)
        {
            toolStripStatusLabelCount.Text = RichTextBox_Workspace.TextLength.ToString();
        }

        private void RichTextBox_TextChanged(object sender, EventArgs e)
        {            
            if (IsSaved)
            {
                IsSaved = false;
                this.Text = "* " + FileName;
            }
        }

        #region CONTEXT MENU EVENTS

        public void Cut_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(RichTextBox_Workspace.SelectedText);
            RichTextBox_Workspace.SelectedText = string.Empty;
        }

        public void Copy_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(RichTextBox_Workspace.SelectedText))
                Clipboard.SetText(RichTextBox_Workspace.SelectedText);
        }

        public void Paste_Click(object sender, EventArgs e)
        {
            if (Clipboard.ContainsText())
            {
                RichTextBox_Workspace.SelectedText = Clipboard.GetText();
            }
        }

        #endregion

        #region EDIT Methods

        public void Undo() => RichTextBox_Workspace.Undo();

        public void Redo() => RichTextBox_Workspace.Redo();

        public void Clear() => RichTextBox_Workspace.Clear();

        public void SelectAll() => RichTextBox_Workspace.SelectAll();

        #endregion

        /// <summary>
        /// Create blank for new file.
        /// </summary>
        /// <param name="fileName"> Name for new file. </param>
        /// <returns></returns>
        public static BlankForm CreateNew(string fileName)
        {
            return new BlankForm
            {
                IsSaved = false,
                FileName = fileName,
                Text = "* " + fileName
            };
        }

        /// <summary>
        /// Create blank for exists file.
        /// </summary>
        /// <param name="filePath"> File path to exists file. </param>
        /// <returns></returns>
        public static BlankForm FromFile(string filePath)
        {
            var blank = new BlankForm
            {
                FullName = filePath,           
                FileName = filePath.Substring(filePath.LastIndexOf('\\') + 1),
                BaseStream = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite),
                IsSaved = true
            };

            blank.Text = blank.FileName;
            blank.ReadStream(blank.BaseStream);
            blank.InitializeEvents();

            return blank;
        }        
    }
}