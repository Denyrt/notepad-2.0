﻿using Notepad.UI;
using System;
using System.Drawing.Printing;
using System.Linq;
using System.Windows.Forms;

namespace Notepad
{
    public partial class MainForm : Form
    {
        public event EventHandler<int> MdiChildrenCountChanged;

        public BlankForm CurrentBlank => this.ActiveMdiChild as BlankForm;

        public MainForm()
        {
            InitializeComponent();
            MdiChildrenCountChanged += OnMdiChildrenCountChanged;           
        }

        private void OnMdiChildrenCountChanged(object sender, int count)
        {
            var enabled = count > 0;

            this.toolStripMenuItem_Save.Enabled = enabled;
            this.toolStripMenuItem_SaveAs.Enabled = enabled;
            this.toolStripButton_Save.Enabled = enabled;

            this.toolStripMenuItem_Close.Enabled = enabled;
            this.toolStripMenuItem_CloseAll.Enabled = enabled;

            this.toolStripButton_Color.Enabled = enabled;
            this.toolStripButton_Font.Enabled = enabled;

            this.toolStripButton_Copy.Enabled = enabled;
            this.toolStripButton_Cut.Enabled = enabled;
            this.toolStripButton_Paste.Enabled = enabled;
            this.toolStripButton_Print.Enabled = enabled;
        }

        private void Blank_Disposed(object sender, EventArgs e)
        {
            MdiChildrenCountChanged?.Invoke(this, base.MdiChildren.Length);
        }

        private void AddBlankToCollection(BlankForm blank)
        {
            blank.MdiParent = this;
            blank.Disposed += Blank_Disposed;
            MdiChildrenCountChanged?.Invoke(this, base.MdiChildren.Length);
            blank.Show();
        }



        #region FILE ToolStripMenuItems Events

        private void ToolStripMenuItem_New_Click(object sender, EventArgs e)
        {
            var blank = BlankForm.CreateNew($"new file {MdiChildren.Length + 1}.txt");
            AddBlankToCollection(blank);                       
        }

        private void ToolStripMenuItem_Open_Click(object sender, EventArgs e)
        {
            OpenFileDialog.ShowDialog();
        }

        private void ToolStripMenuItem_Save_Click(object sender, EventArgs e)
        {
            CurrentBlank?.Save();
        }

        private void ToolStripMenuItem_SaveAs_Click(object sender, EventArgs e)
        {            
            CurrentBlank?.SaveAs();
        }

        private void ToolStripMenuItem_Close_Click(object sender, EventArgs e)
        {
            CurrentBlank?.Close();
        }

        private void ToolStripMenuItem_CloseAll_Click(object sender, EventArgs e)
        {            
            var blanks = this.MdiChildren.OfType<BlankForm>();
            var unsavedCount = blanks.Count(blank => !blank.IsSaved);

            if (unsavedCount > 0)
            {
                var dialog = MessageBox.Show($"You have {unsavedCount} unsaved files. Would you like save them?", "File saving", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                switch (dialog)
                {
                    case DialogResult.Yes:
                        foreach (var blank in blanks) blank.Save();
                        break;
                    case DialogResult.No:
                        foreach (var blank in blanks) blank.CloseWithoutSaving();
                        break;
                    default:
                        break;
                }
            }
        }

        private void ToolStripMenuItem_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        #endregion

        #region LOCATE ToolStripMenuItems Events

        private void ToolStripMenuItem_Cascade_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.Cascade);
        }

        private void ToolStripMenuItem_Horizontal_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ToolStripMenuItem_Vertical_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileVertical);
        }

        #endregion

        #region EDIT ToolStripMenuItems Events

        private void ToolStripMenuItem_Undo_Click(object sender, EventArgs e)
        {
            CurrentBlank?.Undo();
        }

        private void ToolStripMenuItem_Redo_Click(object sender, EventArgs e)
        {
            CurrentBlank?.Redo();
        }

        private void ToolStripMenuItem_Clear_Click(object sender, EventArgs e)
        {
            CurrentBlank?.Clear();
        }

        private void toolStripMenuItem_SelectAll_Click(object sender, EventArgs e)
        {
            CurrentBlank?.SelectAll();
        }

        #endregion

        #region ToolStripShort Events

        private void ToolStripButton_Cut_Click(object sender, EventArgs e)
        {
            CurrentBlank?.Cut_Click(sender, e);
        }

        private void ToolStripButton_Copy_Click(object sender, EventArgs e)
        {
            CurrentBlank?.Copy_Click(sender, e);
        }

        private void ToolStripButton_Paste_Click(object sender, EventArgs e)
        {
            CurrentBlank?.Paste_Click(sender, e);
        }

        private void ToolStripButton_Print_Click(object sender, EventArgs e)
        {
            printDialog.ShowDialog();
        }

        #endregion

        private void OpenFileDialog_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (sender is OpenFileDialog fileDialog)
            {
                var blank = BlankForm.FromFile(fileDialog.FileName);
                AddBlankToCollection(blank);
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ToolStripMenuItem_CloseAll_Click(toolStripMenuItem_CloseAll, new EventArgs());
        }

        private void ToolStripButton_Font_Click(object sender, EventArgs e)
        {
            if (CurrentBlank != null)
            {
                fontDialog.Font = CurrentBlank.RichTextBox_Workspace.SelectionFont;
                
                if (fontDialog.ShowDialog() != DialogResult.OK) return;

                CurrentBlank.RichTextBox_Workspace.SelectionFont = fontDialog.Font;
            }
        }

        private void ToolStripButton_Color_Click(object sender, EventArgs e)
        {
            if (CurrentBlank != null)
            {
                colorDialog.Color = CurrentBlank.RichTextBox_Workspace.SelectionColor;

                if (colorDialog.ShowDialog() != DialogResult.OK) return;

                CurrentBlank.RichTextBox_Workspace.SelectionColor = colorDialog.Color;
            }
        }

        private void ToolStripButton_About_Click(object sender, EventArgs e)
        {
            var aboutForm = new AboutForm();
            aboutForm.ShowDialog();
        }

        private void CustomizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var customizeForm = new CustomizeForm();
            customizeForm.ShowDialog();
        }

        private void ToolStripButton_Help_Click(object sender, EventArgs e)
        {
            var helpForm = new HelpForm();
            helpForm.ShowDialog();
        }

        private void NotifyIcon_Click(object sender, EventArgs e)
        {
            this.notifyIcon.Visible = false;
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == /*WM_SIZE*/ 0x0005)
            {
                if (this.WindowState == FormWindowState.Minimized)
                {
                    ShowInTaskbar = false;
                    notifyIcon.Visible = true;               
                }
            }

            base.WndProc(ref m);
        }
    }
}
