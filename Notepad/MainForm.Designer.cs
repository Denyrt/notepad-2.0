﻿namespace Notepad
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton_File = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripMenuItem_New = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Open = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem_Save = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_SaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem_Close = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_CloseAll = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton_Edit = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripMenuItem_Undo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Redo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Clear = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_SelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton_Tools = new System.Windows.Forms.ToolStripDropDownButton();
            this.customizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Locate = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Cascade = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Horizontal = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Vertical = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButton_Help = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Font = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Color = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_About = new System.Windows.Forms.ToolStripButton();
            this.OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.fontDialog = new System.Windows.Forms.FontDialog();
            this.toolStripShort = new System.Windows.Forms.ToolStrip();
            this.toolStripButton_New = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Open = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Save = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Print = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Cut = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Copy = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Paste = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_HelpShort = new System.Windows.Forms.ToolStripButton();
            this.printDialog = new System.Windows.Forms.PrintDialog();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.toolStrip.SuspendLayout();
            this.toolStripShort.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip
            // 
            this.toolStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton_File,
            this.toolStripDropDownButton_Edit,
            this.toolStripDropDownButton_Tools,
            this.toolStripButton_Help,
            this.toolStripButton_Font,
            this.toolStripButton_Color,
            this.toolStripButton_About});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(982, 27);
            this.toolStrip.TabIndex = 0;
            this.toolStrip.Text = "toolStrip1";
            // 
            // toolStripDropDownButton_File
            // 
            this.toolStripDropDownButton_File.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton_File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_New,
            this.toolStripMenuItem_Open,
            this.toolStripSeparator1,
            this.toolStripMenuItem_Save,
            this.toolStripMenuItem_SaveAs,
            this.toolStripSeparator2,
            this.toolStripMenuItem_Close,
            this.toolStripMenuItem_CloseAll,
            this.toolStripSeparator3,
            this.toolStripMenuItem_Exit});
            this.toolStripDropDownButton_File.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton_File.Image")));
            this.toolStripDropDownButton_File.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton_File.Name = "toolStripDropDownButton_File";
            this.toolStripDropDownButton_File.Size = new System.Drawing.Size(46, 24);
            this.toolStripDropDownButton_File.Text = "&File";
            // 
            // toolStripMenuItem_New
            // 
            this.toolStripMenuItem_New.Name = "toolStripMenuItem_New";
            this.toolStripMenuItem_New.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.toolStripMenuItem_New.Size = new System.Drawing.Size(244, 26);
            this.toolStripMenuItem_New.Text = "&New";
            this.toolStripMenuItem_New.Click += new System.EventHandler(this.ToolStripMenuItem_New_Click);
            // 
            // toolStripMenuItem_Open
            // 
            this.toolStripMenuItem_Open.Name = "toolStripMenuItem_Open";
            this.toolStripMenuItem_Open.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.toolStripMenuItem_Open.Size = new System.Drawing.Size(244, 26);
            this.toolStripMenuItem_Open.Text = "&Open";
            this.toolStripMenuItem_Open.Click += new System.EventHandler(this.ToolStripMenuItem_Open_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(241, 6);
            // 
            // toolStripMenuItem_Save
            // 
            this.toolStripMenuItem_Save.Enabled = false;
            this.toolStripMenuItem_Save.Name = "toolStripMenuItem_Save";
            this.toolStripMenuItem_Save.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.toolStripMenuItem_Save.Size = new System.Drawing.Size(244, 26);
            this.toolStripMenuItem_Save.Text = "Save";
            this.toolStripMenuItem_Save.Click += new System.EventHandler(this.ToolStripMenuItem_Save_Click);
            // 
            // toolStripMenuItem_SaveAs
            // 
            this.toolStripMenuItem_SaveAs.Enabled = false;
            this.toolStripMenuItem_SaveAs.Name = "toolStripMenuItem_SaveAs";
            this.toolStripMenuItem_SaveAs.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.toolStripMenuItem_SaveAs.Size = new System.Drawing.Size(244, 26);
            this.toolStripMenuItem_SaveAs.Text = "Save &as ...";
            this.toolStripMenuItem_SaveAs.Click += new System.EventHandler(this.ToolStripMenuItem_SaveAs_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(241, 6);
            // 
            // toolStripMenuItem_Close
            // 
            this.toolStripMenuItem_Close.Enabled = false;
            this.toolStripMenuItem_Close.Name = "toolStripMenuItem_Close";
            this.toolStripMenuItem_Close.Size = new System.Drawing.Size(244, 26);
            this.toolStripMenuItem_Close.Text = "Close";
            this.toolStripMenuItem_Close.Click += new System.EventHandler(this.ToolStripMenuItem_Close_Click);
            // 
            // toolStripMenuItem_CloseAll
            // 
            this.toolStripMenuItem_CloseAll.Enabled = false;
            this.toolStripMenuItem_CloseAll.Name = "toolStripMenuItem_CloseAll";
            this.toolStripMenuItem_CloseAll.Size = new System.Drawing.Size(244, 26);
            this.toolStripMenuItem_CloseAll.Text = "Close all";
            this.toolStripMenuItem_CloseAll.Click += new System.EventHandler(this.ToolStripMenuItem_CloseAll_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(241, 6);
            // 
            // toolStripMenuItem_Exit
            // 
            this.toolStripMenuItem_Exit.Name = "toolStripMenuItem_Exit";
            this.toolStripMenuItem_Exit.Size = new System.Drawing.Size(244, 26);
            this.toolStripMenuItem_Exit.Text = "Exit";
            this.toolStripMenuItem_Exit.Click += new System.EventHandler(this.ToolStripMenuItem_Exit_Click);
            // 
            // toolStripDropDownButton_Edit
            // 
            this.toolStripDropDownButton_Edit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton_Edit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_Undo,
            this.toolStripMenuItem_Redo,
            this.toolStripMenuItem_Clear,
            this.toolStripMenuItem_SelectAll});
            this.toolStripDropDownButton_Edit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton_Edit.Image")));
            this.toolStripDropDownButton_Edit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton_Edit.Name = "toolStripDropDownButton_Edit";
            this.toolStripDropDownButton_Edit.Size = new System.Drawing.Size(49, 24);
            this.toolStripDropDownButton_Edit.Text = "&Edit";
            // 
            // toolStripMenuItem_Undo
            // 
            this.toolStripMenuItem_Undo.Name = "toolStripMenuItem_Undo";
            this.toolStripMenuItem_Undo.Size = new System.Drawing.Size(154, 26);
            this.toolStripMenuItem_Undo.Text = "Undo";
            this.toolStripMenuItem_Undo.Click += new System.EventHandler(this.ToolStripMenuItem_Undo_Click);
            // 
            // toolStripMenuItem_Redo
            // 
            this.toolStripMenuItem_Redo.Name = "toolStripMenuItem_Redo";
            this.toolStripMenuItem_Redo.Size = new System.Drawing.Size(154, 26);
            this.toolStripMenuItem_Redo.Text = "Redo";
            this.toolStripMenuItem_Redo.Click += new System.EventHandler(this.ToolStripMenuItem_Redo_Click);
            // 
            // toolStripMenuItem_Clear
            // 
            this.toolStripMenuItem_Clear.Name = "toolStripMenuItem_Clear";
            this.toolStripMenuItem_Clear.Size = new System.Drawing.Size(154, 26);
            this.toolStripMenuItem_Clear.Text = "Clear";
            this.toolStripMenuItem_Clear.Click += new System.EventHandler(this.ToolStripMenuItem_Clear_Click);
            // 
            // toolStripMenuItem_SelectAll
            // 
            this.toolStripMenuItem_SelectAll.Name = "toolStripMenuItem_SelectAll";
            this.toolStripMenuItem_SelectAll.Size = new System.Drawing.Size(154, 26);
            this.toolStripMenuItem_SelectAll.Text = "Select All";
            this.toolStripMenuItem_SelectAll.Click += new System.EventHandler(this.toolStripMenuItem_SelectAll_Click);
            // 
            // toolStripDropDownButton_Tools
            // 
            this.toolStripDropDownButton_Tools.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton_Tools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customizeToolStripMenuItem,
            this.optionsToolStripMenuItem,
            this.toolStripMenuItem_Locate});
            this.toolStripDropDownButton_Tools.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton_Tools.Image")));
            this.toolStripDropDownButton_Tools.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton_Tools.Name = "toolStripDropDownButton_Tools";
            this.toolStripDropDownButton_Tools.Size = new System.Drawing.Size(58, 24);
            this.toolStripDropDownButton_Tools.Text = "&Tools";
            // 
            // customizeToolStripMenuItem
            // 
            this.customizeToolStripMenuItem.Name = "customizeToolStripMenuItem";
            this.customizeToolStripMenuItem.Size = new System.Drawing.Size(161, 26);
            this.customizeToolStripMenuItem.Text = "&Customize";
            this.customizeToolStripMenuItem.Click += new System.EventHandler(this.CustomizeToolStripMenuItem_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(161, 26);
            this.optionsToolStripMenuItem.Text = "&Options";
            // 
            // toolStripMenuItem_Locate
            // 
            this.toolStripMenuItem_Locate.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_Cascade,
            this.toolStripMenuItem_Horizontal,
            this.toolStripMenuItem_Vertical});
            this.toolStripMenuItem_Locate.Name = "toolStripMenuItem_Locate";
            this.toolStripMenuItem_Locate.Size = new System.Drawing.Size(161, 26);
            this.toolStripMenuItem_Locate.Text = "Locate";
            // 
            // toolStripMenuItem_Cascade
            // 
            this.toolStripMenuItem_Cascade.Name = "toolStripMenuItem_Cascade";
            this.toolStripMenuItem_Cascade.Size = new System.Drawing.Size(162, 26);
            this.toolStripMenuItem_Cascade.Text = "Cascade";
            this.toolStripMenuItem_Cascade.Click += new System.EventHandler(this.ToolStripMenuItem_Cascade_Click);
            // 
            // toolStripMenuItem_Horizontal
            // 
            this.toolStripMenuItem_Horizontal.Name = "toolStripMenuItem_Horizontal";
            this.toolStripMenuItem_Horizontal.Size = new System.Drawing.Size(162, 26);
            this.toolStripMenuItem_Horizontal.Text = "Horizontal";
            this.toolStripMenuItem_Horizontal.Click += new System.EventHandler(this.ToolStripMenuItem_Horizontal_Click);
            // 
            // toolStripMenuItem_Vertical
            // 
            this.toolStripMenuItem_Vertical.Name = "toolStripMenuItem_Vertical";
            this.toolStripMenuItem_Vertical.Size = new System.Drawing.Size(162, 26);
            this.toolStripMenuItem_Vertical.Text = "Vertical";
            this.toolStripMenuItem_Vertical.Click += new System.EventHandler(this.ToolStripMenuItem_Vertical_Click);
            // 
            // toolStripButton_Help
            // 
            this.toolStripButton_Help.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_Help.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Help.Image")));
            this.toolStripButton_Help.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Help.Name = "toolStripButton_Help";
            this.toolStripButton_Help.Size = new System.Drawing.Size(45, 24);
            this.toolStripButton_Help.Text = "&Help";
            this.toolStripButton_Help.Click += new System.EventHandler(this.ToolStripButton_Help_Click);
            // 
            // toolStripButton_Font
            // 
            this.toolStripButton_Font.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_Font.Enabled = false;
            this.toolStripButton_Font.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Font.Image")));
            this.toolStripButton_Font.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Font.Name = "toolStripButton_Font";
            this.toolStripButton_Font.Size = new System.Drawing.Size(42, 24);
            this.toolStripButton_Font.Text = "Font";
            this.toolStripButton_Font.Click += new System.EventHandler(this.ToolStripButton_Font_Click);
            // 
            // toolStripButton_Color
            // 
            this.toolStripButton_Color.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_Color.Enabled = false;
            this.toolStripButton_Color.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Color.Image")));
            this.toolStripButton_Color.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Color.Name = "toolStripButton_Color";
            this.toolStripButton_Color.Size = new System.Drawing.Size(49, 24);
            this.toolStripButton_Color.Text = "Color";
            this.toolStripButton_Color.Click += new System.EventHandler(this.ToolStripButton_Color_Click);
            // 
            // toolStripButton_About
            // 
            this.toolStripButton_About.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_About.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_About.Image")));
            this.toolStripButton_About.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_About.Name = "toolStripButton_About";
            this.toolStripButton_About.Size = new System.Drawing.Size(54, 24);
            this.toolStripButton_About.Text = "About";
            this.toolStripButton_About.Click += new System.EventHandler(this.ToolStripButton_About_Click);
            // 
            // OpenFileDialog
            // 
            this.OpenFileDialog.Filter = "txt files (*.txt)|*.txt";
            this.OpenFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.OpenFileDialog_FileOk);
            // 
            // toolStripShort
            // 
            this.toolStripShort.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStripShort.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton_New,
            this.toolStripButton_Open,
            this.toolStripButton_Save,
            this.toolStripButton_Print,
            this.toolStripSeparator,
            this.toolStripButton_Cut,
            this.toolStripButton_Copy,
            this.toolStripButton_Paste,
            this.toolStripSeparator4,
            this.toolStripButton_HelpShort});
            this.toolStripShort.Location = new System.Drawing.Point(0, 27);
            this.toolStripShort.Name = "toolStripShort";
            this.toolStripShort.Size = new System.Drawing.Size(982, 27);
            this.toolStripShort.TabIndex = 2;
            this.toolStripShort.Text = "toolStrip1";
            // 
            // toolStripButton_New
            // 
            this.toolStripButton_New.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_New.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_New.Image")));
            this.toolStripButton_New.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_New.Name = "toolStripButton_New";
            this.toolStripButton_New.Size = new System.Drawing.Size(29, 24);
            this.toolStripButton_New.Text = "&Создать";
            this.toolStripButton_New.Click += new System.EventHandler(this.ToolStripMenuItem_New_Click);
            // 
            // toolStripButton_Open
            // 
            this.toolStripButton_Open.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Open.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Open.Image")));
            this.toolStripButton_Open.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Open.Name = "toolStripButton_Open";
            this.toolStripButton_Open.Size = new System.Drawing.Size(29, 24);
            this.toolStripButton_Open.Text = "&Открыть";
            this.toolStripButton_Open.Click += new System.EventHandler(this.ToolStripMenuItem_Open_Click);
            // 
            // toolStripButton_Save
            // 
            this.toolStripButton_Save.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Save.Enabled = false;
            this.toolStripButton_Save.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Save.Image")));
            this.toolStripButton_Save.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Save.Name = "toolStripButton_Save";
            this.toolStripButton_Save.Size = new System.Drawing.Size(29, 24);
            this.toolStripButton_Save.Text = "&Сохранить";
            this.toolStripButton_Save.Click += new System.EventHandler(this.ToolStripMenuItem_Save_Click);
            // 
            // toolStripButton_Print
            // 
            this.toolStripButton_Print.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Print.Enabled = false;
            this.toolStripButton_Print.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Print.Image")));
            this.toolStripButton_Print.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Print.Name = "toolStripButton_Print";
            this.toolStripButton_Print.Size = new System.Drawing.Size(29, 24);
            this.toolStripButton_Print.Text = "&Печать";
            this.toolStripButton_Print.Click += new System.EventHandler(this.ToolStripButton_Print_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 27);
            // 
            // toolStripButton_Cut
            // 
            this.toolStripButton_Cut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Cut.Enabled = false;
            this.toolStripButton_Cut.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Cut.Image")));
            this.toolStripButton_Cut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Cut.Name = "toolStripButton_Cut";
            this.toolStripButton_Cut.Size = new System.Drawing.Size(29, 24);
            this.toolStripButton_Cut.Text = "В&ырезать";
            this.toolStripButton_Cut.Click += new System.EventHandler(this.ToolStripButton_Cut_Click);
            // 
            // toolStripButton_Copy
            // 
            this.toolStripButton_Copy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Copy.Enabled = false;
            this.toolStripButton_Copy.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Copy.Image")));
            this.toolStripButton_Copy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Copy.Name = "toolStripButton_Copy";
            this.toolStripButton_Copy.Size = new System.Drawing.Size(29, 24);
            this.toolStripButton_Copy.Text = "&Копировать";
            this.toolStripButton_Copy.Click += new System.EventHandler(this.ToolStripButton_Copy_Click);
            // 
            // toolStripButton_Paste
            // 
            this.toolStripButton_Paste.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_Paste.Enabled = false;
            this.toolStripButton_Paste.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Paste.Image")));
            this.toolStripButton_Paste.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Paste.Name = "toolStripButton_Paste";
            this.toolStripButton_Paste.Size = new System.Drawing.Size(29, 24);
            this.toolStripButton_Paste.Text = "Вст&авка";
            this.toolStripButton_Paste.Click += new System.EventHandler(this.ToolStripButton_Paste_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 27);
            // 
            // toolStripButton_HelpShort
            // 
            this.toolStripButton_HelpShort.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_HelpShort.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_HelpShort.Image")));
            this.toolStripButton_HelpShort.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_HelpShort.Name = "toolStripButton_HelpShort";
            this.toolStripButton_HelpShort.Size = new System.Drawing.Size(29, 24);
            this.toolStripButton_HelpShort.Text = "Спр&авка";
            this.toolStripButton_HelpShort.Click += new System.EventHandler(this.ToolStripButton_Help_Click);
            // 
            // printDialog
            // 
            this.printDialog.UseEXDialog = true;
            // 
            // notifyIcon
            // 
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "Notepad";
            this.notifyIcon.Click += new System.EventHandler(this.NotifyIcon_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 553);
            this.Controls.Add(this.toolStripShort);
            this.Controls.Add(this.toolStrip);
            this.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Notepad";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.toolStripShort.ResumeLayout(false);
            this.toolStripShort.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton toolStripButton_Help;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton_Tools;
        private System.Windows.Forms.ToolStripMenuItem customizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton_File;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_New;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Open;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Save;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_SaveAs;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Close;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_CloseAll;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Locate;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Cascade;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Horizontal;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Vertical;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.OpenFileDialog OpenFileDialog;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton_Edit;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Undo;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Clear;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Redo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Exit;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_SelectAll;
        private System.Windows.Forms.ToolStripButton toolStripButton_Font;
        private System.Windows.Forms.ToolStripButton toolStripButton_Color;
        private System.Windows.Forms.ToolStripButton toolStripButton_About;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.FontDialog fontDialog;
        private System.Windows.Forms.ToolStrip toolStripShort;
        private System.Windows.Forms.ToolStripButton toolStripButton_New;
        private System.Windows.Forms.ToolStripButton toolStripButton_Open;
        private System.Windows.Forms.ToolStripButton toolStripButton_Save;
        private System.Windows.Forms.ToolStripButton toolStripButton_Print;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripButton toolStripButton_Cut;
        private System.Windows.Forms.ToolStripButton toolStripButton_Copy;
        private System.Windows.Forms.ToolStripButton toolStripButton_Paste;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripButton_HelpShort;
        private System.Windows.Forms.PrintDialog printDialog;
        private System.Windows.Forms.NotifyIcon notifyIcon;
    }
}

